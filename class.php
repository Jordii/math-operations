<?php

class mathOperations{

	protected $firstValue = null;
	protected $secondValue = null;
	protected $result = null;


	public function __construct(){

	}

	//haal de eerste waarde op
	public function getfirstValue() {
		return $this->firstValue;
	}

	//haal de tweede waarde op
	public function getsecondValue() {
		return $this->secondValue;
	}

}
	

	class Sum extends mathOperations {


		public function __construct($firstValue, $secondValue){
			$this->firstValue = $firstValue;
			$this->secondValue = $secondValue;
		}

		public function calculation(){
			$this->result = $this->firstValue + $this->secondValue;
			return $this->result;
		}

	}



	class subtract extends mathOperations {

		public function __construct($firstValue, $secondValue){
			$this->firstValue = $firstValue;
			$this->secondValue = $secondValue;
		}

		public function calculation(){
			$this->result = $this->firstValue - $this->secondValue;
			return $this->result;
		}
	}




	class multiply extends mathOperations {

		public function __construct($firstValue, $secondValue){
			$this->firstValue = $firstValue;
			$this->secondValue = $secondValue;
		}

		public function calculation(){
			$this->result = $this->firstValue * $this->secondValue;
			return $this->result;
		}
	}

?>